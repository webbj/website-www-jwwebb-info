---
layout: blog-article.njk
title: Running Headless Chrome as a Lando Service
tags: ['lando', 'chrome', 'behat', 'testing', 'containers']
published: "2024-02-19T12:00-05:00"
image: "/blog/2024/02/19/headless-chrome-for-lando/chrome-in-lando-514x514.png"
summary: >-
  Straight to the point instructions on configuraing a headless chrome services
  within Lando for use with Behat or similar browser automation and testing 
  tools. 
cannonical: "https://www.jwwebb.info/blog/2024/02/19/headless-chrome-for-lando/"
---

Here is a YAML snippet intended to include a headless Chromium service for sites using Lando for development. The snippet can be nested under the top-level `services` element in the `.lando.yml` file and worked for me using [Lando 3.20.8](https://github.com/lando/lando/releases/tag/v3.20.8) and [zenika/alpine-chrome:121](https://hub.docker.com/r/zenika/alpine-chrome). You may also need to run `lando start` or `lando rebuild` to spin up this new container service.

```yaml
  # Headless Chrome Lando Service
  chrome:
    api: 3
    type: lando
    services:
      image: zenika/alpine-chrome:latest
      command: >
        "
        chromium-browser
        --headless=chrome --disable-gpu --no-sandbox
        --remote-debugging-address=0.0.0.0 --remote-debugging-port=9222
        "
      ports:
        - "9222:9222"
      volumes:
        - /dev/shm:/dev/shm
      user: root
      environment:
        LANDO_DROP_USER: chrome
```

## Confirm the Lando App can connect to the Chrome Debugger

Curl can be used to verify that the Lando app service can connect to Chrome over the remote debugging port. I personally find the container network details mystifying, but passing 'localhost' as the hostname whilst connecting to 'chrome' server name seems to make this work.

```
jw@laptop $ lando ssh -c "curl -s -H 'Host: localhost' http://chrome:9222/json/version"
{
   "Browser": "HeadlessChrome/121.0.6167.184",
   "Protocol-Version": "1.3",
   "User-Agent": "Mozilla/5.0 (X11; Linux aarch64) AppleWebKit/537.36 (KHTML, like Gecko) HeadlessChrome/121.0.6167.184 Safari/537.36",
   "V8-Version": "12.1.285.28",
   "WebKit-Version": "537.36 (@057a8ae7deb3374d0f1b04b36304d236f0136188)",
   "webSocketDebuggerUrl": "ws://localhost/devtools/browser/24dcbafb-6516-4b1a-94ab-7b4dc003329d"
}
```

## A note on headless modes

At the time of writing, `--headless=new` is supposed to be the preferred mode for invoking headless Chrome, however in my testing only `--hedless=chrome` and `--headless=old` seem to work as options. The issue appears to be with changes to the `--remote-debugging-address` circa Chrome 109.

## References

- [Lando 101: Lando Services](https://docs.lando.dev/lando-101/lando-services.html)
- [Lando Service](https://docs.lando.dev/core/v3/lando-service.html)
- [Chrome's Headless mode gets an upgrade](https://developer.chrome.com/docs/chromium/new-headless)
- [chromium/chromium@e9c51611](https://github.com/chromium/chromium/commit/e9c51611)
- [headless-new not support for --remote-debug-address=0.0.0.0 ](https://issues.chromium.org/issues/41487651)
- [Flag `--remote-debugging-address` does not work as expected](https://issues.chromium.org/issues/41487252)

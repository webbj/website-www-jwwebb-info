---
layout: mylayout.njk
title: J. W. Webb
---
# {{ title }}

## Latest Posts

- 2024-02-19: [Running Headless Chrome as a Lando Service](/blog/2024/02/19/headless-chrome-for-lando/)
